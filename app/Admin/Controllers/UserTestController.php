<?php

namespace App\Admin\Controllers;

use App\UserTest;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class UserTestController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'QL User';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new UserTest);

        $grid->column('id', __('Id'));
        $grid->column('full_name', __('Full name'));
        $grid->column('address', __('Address'));
        $grid->avatar()->image('../image_upload',100,50);
        $grid->column('age', __('Age'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(UserTest::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('full_name', __('Full name'));
        $show->field('address', __('Address'));
        $show->field('avatar', __('Avatar'));
        $show->field('age', __('Age'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new UserTest);

         $form->text('full_name', 'Tên')->rules('required|min:3', [
			    'required' => 'Không được bỏ trống tên',
			    'min'   => 'Tên phải có ít nhất 3 ký tự',
				]);


        $form->text('address', 'Địa chỉ')->rules('required', [
			    'required' => 'Không được bỏ trống địa chỉ',
				]);

        $form->number('age', __('Age'));
        $form->image('avatar', 'Ảnh')->move('image_upload');

        // tắt footer form
    	// disable select footer
    	$form->footer(function ($footer) {

	    // disable `View` checkbox
	    $footer->disableViewCheck();

	    // disable `Continue editing` checkbox
	    $footer->disableEditingCheck();

	    // disable `Continue Creating` checkbox
	    $footer->disableCreatingCheck();

		});
        

        return $form;
    }
}
