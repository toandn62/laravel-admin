<?php

namespace App\Admin\Controllers;

use App\nguoi_dung;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid; // sử dụng lưới

use Encore\Admin\Facades\Admin;

use Encore\Admin\Show;

use Illuminate\Support\MessageBag; // hiển thị thông tin trong quá trình gửi dữ liệu


use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;

class nguoidungController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Quản lý người dùng';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new nguoi_dung);

        $grid->column('id', __('Id'));
        $grid->column('full_name', __('Full name'));
        $grid->column('age', __('Age'));
        $grid->column('address', __('Address'));
        $grid->avatar()->image(100,50);
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(nguoi_dung::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('full_name', __('Full name'));
        $show->field('age', __('Age'));
        $show->field('address', __('Address'));
        $show->field('avatar', __('Avatar'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new nguoi_dung);

        $form->text('full_name', __('Full name'));
        $form->number('age', __('Age'));
        $form->text('address', __('Address'));
        $form->image('avatar', __('Avatar'));

        return $form;
    }











    // đoạn này nghịch. không có tác dụng gì //
    ///////////////////////////////////////////
    //////////////////////////////////////////
    /////////////////////////////////////////

    protected function test() {
    	return Admin::content(function (Content $content) {

        // Tiêu đề
        $content->header('Người Dùng');

        // Mô tả
        $content->description('user');
        // Nội dung
        $content->body(
        	// khởi tạo bảng sử dụng model nguoi_dung
        	$grid = Admin::grid(nguoi_dung::class, function(Grid $grid){
        		$grid->id('ID')->sortable();
        		$grid->column('full_name', 'Họ và tên')->color('blue');
	    		$grid->column('age', 'Tuổi');
	    		$grid->column('address', 'Địa chỉ');
	    		// hiển thị ảnh
	    		$grid->avatar()->image(100,50);
        		$grid->column('created_at', 'Ngày tạo');
        		$grid->column('updated_at', 'Ngày sửa');
        		})
        	);
    	});
    }

	protected function add(){
		return Admin::content(function (Content $content) {

        // Tiêu đề
        $content->header('Create');

        // Mô tả
        $content->description('Thêm mới');
        // Nội dung
        $content->body(
        	// khởi tạo bảng sử dụng model nguoi_dung
		    $form = Admin::form(nguoi_dung::class, function(Form $form){

		    //hiển thị form + điều kiện form
		    $form->text('full_name', 'Tên')->rules('required|min:3', [
			    'required' => 'Không được bỏ trống tên',
			    'min'   => 'Tên phải có ít nhất 3 ký tự',
				]);

        	$form->number('age', 'Tuổi');

        	$form->text('address', 'Địa chỉ')->rules('required', [
			    'required' => 'Không được bỏ trống địa chỉ',
				]);

        	$form->image('avatar', 'Ảnh')->move('/image_upload');



			








        	// tắt footer form
        	// disable select footer
        	$form->footer(function ($footer) {

		    // disable `View` checkbox
		    $footer->disableViewCheck();

		    // disable `Continue editing` checkbox
		    $footer->disableEditingCheck();

		    // disable `Continue Creating` checkbox
		    $footer->disableCreatingCheck();

			});


			})
        );
    	});
	}
	protected function test2() {
		$form = new Form(new nguoi_dung);
		$form->text('full_name', 'Tên');
		$form->number('age', 'Tuổi');
		$form->text('address', 'Địa chỉ');
		$form->image('avatar', 'Ảnh');

		return $form;
		

		//return $grid->render();
	}
}
