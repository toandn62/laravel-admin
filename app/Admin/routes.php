<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('admin.home');
    //$router->get('nguoidung', 'nguoidungController@test');
    //$router->get('nguoidung/create', 'nguoidungController@add');
    //$router->get('test', 'nguoidungController@test2');


    $router->resource('user-test', UserTestController::class);
    

});
